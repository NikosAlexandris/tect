Version History
=========================
### Version: 1.1.17
* Included Ubuntu Light font.

### Version: 1.1.16
* Allow SVGs in media library.

### Version: 1.1.15
* Included Gentium font.

### Version: 1.1.14
* Polished alternate media file structure. Safe to deploy once again.

### Version: 1.1.13
DO NOT UPDATE to this version, unless you know what you’re doing.

* Implemented custom thumbnail naming functions.
* Begin logging version changes.
